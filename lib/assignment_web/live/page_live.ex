defmodule AssignmentWeb.PageLive do
  use AssignmentWeb, :live_view

  @impl true
  def mount(params, _session, socket) do
    if params["q"] do
      {:ok, assign(socket, query: params["q"], results: search(params["q"]))}
    else
      {:ok, assign(socket, query: "", results: [])}
    end
  end

  @impl true
  def handle_event("suggest", %{"q" => ""}, socket) do
    {:noreply, assign(socket, results: [], query: "")}
  end
  def handle_event("suggest", %{"q" => query}, socket) do
    results = search(query)
    socket = if Enum.count(results) === 0 do
      put_flash(socket, :error, "No result found. Maybe throttled?")
    else
      socket
    end
    {:noreply, assign(socket, results: results, query: query)}
  end

  defp search(""), do: []
  defp search(nil), do: []
  defp search(query) do
    q = URI.encode_www_form(query)
    {:ok, result} = Finch.build(:get, "https://api.github.com/search/repositories?q=#{q}&per_page=10") |> Finch.request(HttpClient)

    decoded = Jason.decode!(result.body)
    case decoded["items"] do
      l when is_list(l) -> l

      # Some error happened with GitHub API, maybe throttled
      _others -> []
    end
  end
end
